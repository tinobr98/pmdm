package dam.tino.ejercicio2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

    public class LogActivity extends AppCompatActivity {
        private static final String DEBUG_TAG = "LOG- "+ "NextActivity";


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_log);
        }

        @Override
        protected void onStart () {
            super.onStart();
            Log.i(DEBUG_TAG, "OnStar");
        }

        @Override
        protected void onStop () {
            super.onStop();
            Log.i(DEBUG_TAG, "onStop");

        }

        @Override
        protected void onPause () {
            super.onPause();
            Log.i(DEBUG_TAG, "onPause");

        }

        @Override
        protected void onResume () {
            super.onResume();
            Log.i(DEBUG_TAG, "onResume");

        }

        @Override
        protected void onRestart () {
            super.onRestart();
            Log.i(DEBUG_TAG, "onRestart");

        }

        @Override
        protected void onDestroy () {
            super.onDestroy();
            Log.i(DEBUG_TAG, "onDestroy");

        }
        @Override
        public boolean isFinishing() {
            return super.isFinishing();
        }

        @Override
        protected void onRestoreInstanceState(Bundle savedInstanceState){
            super.onRestoreInstanceState(savedInstanceState);
        }

        @Override
        protected void onSaveInstanceState(Bundle outState){
            super.onSaveInstanceState(outState);
        }
    }

