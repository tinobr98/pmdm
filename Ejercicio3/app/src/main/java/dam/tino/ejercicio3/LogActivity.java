package dam.tino.ejercicio3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

public class LogActivity extends AppCompatActivity {
    private static final String DEBUG_TAG = "LOG- "+ "NextActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);
    }

    @Override
    protected void onStart () {
        super.onStart();
        Log.i(DEBUG_TAG, "OnStart");
        notify("OnStart");
    }

    @Override
    protected void onStop () {
        super.onStop();
        Log.i(DEBUG_TAG, "onStop");
        notify("onStop");


    }

    @Override
    protected void onPause () {
        super.onPause();
        Log.i(DEBUG_TAG, "onPause");
        notify("onPause");


    }

    @Override
    protected void onResume () {
        super.onResume();
        Log.i(DEBUG_TAG, "onResume");
        notify("onResume");

    }

    @Override
    protected void onRestart () {
        super.onRestart();
        Log.i(DEBUG_TAG, "onRestart");
        notify("onRestart");

    }

    @Override
    protected void onDestroy () {
        super.onDestroy();
        Log.i(DEBUG_TAG, "onDestroy");
        notify("onDestroy");

    }
    @Override
    public boolean isFinishing() {
        notify("isFinishing");
        return super.isFinishing();

    }

    private void notify(String eventName){
        String activityName= this.getClass().getSimpleName();

        String CHANNEL_ID= "My_LifeCycle";

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, "My_LifeCycle",
                    NotificationManager.IMPORTANCE_DEFAULT);

            notificationChannel.setDescription("lifecycle events");
            notificationChannel.setShowBadge(true);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);


            if(notificationManager != null){
                notificationManager.createNotificationChannel(notificationChannel);
            }

        }

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);

        NotificationCompat.Builder notificatioinBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(eventName+ " "+activityName)
                .setContentText(getPackageName())
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher);

        notificationManagerCompat.notify((int) System.currentTimeMillis(),notificatioinBuilder.build());

    }
}


