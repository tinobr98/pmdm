package dam.tino.ejercicio1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends LogActivity {
    public static final String DEBUG_TAG = "LogsAndroid-1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i(DEBUG_TAG, "onCreate");
        setupUI();
    }


    public void LaunchNextActivity(View view) {
        startActivity(new Intent(this, NextActivity.class));
    }

    private void setupUI(){
        Button btNextActivity_2 = findViewById(R.id.btNextActivity_2);

        btNextActivity_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, NextActivity_2.class));
            }
        });
    }


}